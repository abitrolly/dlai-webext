## Web Extension to download short courses offline viewing

This web extension https://learn.deeplearning.ai adds a button to download
video, subtitle and notebook from a current page for offline study.


## Development

[The manual](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/user_interface/Page_actions).
To execute development version:

```
web-ext run
```



